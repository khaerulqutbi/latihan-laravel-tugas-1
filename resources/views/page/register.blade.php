@extends('layout.master')
@section('title')
    Buat Akun Baru
@endsection

@section('content')
    <form action="/kirim" method="POST">
        @csrf
        <h2>Sign Up Form</h2>
        <p>First Name : </p>
        <input name="firstname" type="text">
        <p>Last Name : </p>
        <input name="lastname" type="text">
        <p>Gender</p>
        <input type="radio" name="gender" id="male" value="male"><label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="female"><label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other"><label for="other">Other</label><br>
        <p>Nationality</p>
        <select name="national" id="national">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">malaysia</option>
            <option value="other">Other</option>
        </select>
        <p>Bio</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Submit">
    </form>
@endsection


    
