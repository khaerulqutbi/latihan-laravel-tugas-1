<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('page.register');
    }

    public function kirim(Request $request) {
        // dd($request->all());
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $gender = $request['gender'];
        $national = $request['national'];
        $bio = $request['bio'];

        return view('page.welcome', compact("firstname", "lastname", "gender", "national", "bio"));
    }
}
